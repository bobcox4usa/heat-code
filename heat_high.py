import time 
import RPi.GPIO as io 

io.setwarnings(False)
io.setmode(io.BCM) 

on_off_pin = 18 
hi_low_pin = 23

io.setup(on_off_pin, io.OUT)
io.setup(hi_low_pin, io.OUT)

time.sleep(1)

io.output(hi_low_pin, True)
time.sleep(6)
io.output(hi_low_pin, False)
print("Heat High")

