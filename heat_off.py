import time 
import RPi.GPIO as io 

io.setwarnings(False)
io.setmode(io.BCM) 

on_off_pin = 18 
hi_low_pin = 23

io.setup(on_off_pin, io.OUT)
io.setup(hi_low_pin, io.OUT)

time.sleep(1)
        
io.output(on_off_pin, False)
print("Heat Off")
        
time.sleep(1)

io.cleanup()
